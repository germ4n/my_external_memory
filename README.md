# My External Memory

A simple application to manage a set of notes, lists, to-dos, and frequently used scripts for daily use.

The application is simple by design. It aims to do only one thing but do it very well.

For the interface, I used the incredible [egui](https://github.com/emilk/egui/) library.
And after integrating [egui_dock](https://github.com/Adanos020/egui_dock) the app have tabs.

## Preview

![Screen shot 1](media/screenshot.png)
