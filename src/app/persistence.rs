use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::{self, Read, Write};

/// Runner that can be used to run an script
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Runner {
    /// Name of the runner
    pub name: String,
    /// Code of the runner
    pub code: String,
}

/// An script that can be runned
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Script {
    /// Name of the script
    pub name: String,
    /// Code of the script
    pub code: String,
    /// Name of the runner used to run the script
    pub runner: String,
}

/// An idea
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Idea {
    /// Name of the idea
    pub name: String,
    /// Content of the idea
    pub content: String,
}

/// Workspace data
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Workspace {
    /// Collection of runners
    pub runners: Vec<Runner>,
    /// Collection of scripts
    pub scripts: Vec<Script>,
    /// Collection of ideas
    pub ideas: Vec<Idea>,
}

/// Load a workspace data from a Json file
pub fn load_json(filename: &str) -> io::Result<Workspace> {
    let mut file = File::open(filename)?;

    let mut json_string = String::new();
    file.read_to_string(&mut json_string)?;

    let data: Workspace = serde_json::from_str(&json_string)?;

    Ok(data)
}

/// Store a workspace data in a Json file
pub fn save_json(filename: &str, data: &Workspace) -> io::Result<()> {
    let json_string = serde_json::to_string_pretty(data)?;

    let mut file = File::create(filename)?;
    file.write_all(json_string.as_bytes())?;

    Ok(())
}
