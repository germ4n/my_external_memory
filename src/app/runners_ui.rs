use super::{persistence, Buffers};
use egui::OutputCommand;

/// UI for Runners
pub fn runners_gui(ui: &mut egui::Ui, storage: &mut Buffers) {
    ui.horizontal(|ui| {
        if ui.button("➕").clicked() {
            storage.workspace.runners.push(persistence::Runner {
                name: "".to_owned(),
                code: "".to_owned(),
            });
        }
        ui.separator();
        ui.label("Filter: ");
        ui.text_edit_singleline(&mut storage.filter);
    });

    ui.separator();

    runners_table_gui(ui, storage);
}

fn runners_table_gui(ui: &mut egui::Ui, storage: &mut Buffers) {
    use egui_extras::{Column, TableBuilder};

    let mut remove_item = false;
    let mut remove_idx = 0;

    let table = TableBuilder::new(ui)
        .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
        .column(Column::auto())
        .column(Column::auto())
        .column(Column::remainder())
        .min_scrolled_height(0.0);

    table
        .header(20.0, |mut header| {
            header.col(|ui| {
                ui.strong("Name");
            });
            header.col(|ui| {
                ui.strong("Actions");
            });
            header.col(|ui| {
                ui.strong("Code");
            });
        })
        .body(|mut body| {
            for (idx, item) in storage.workspace.runners.iter_mut().enumerate() {
                if item.name.contains(&storage.filter) {
                    body.row(18.0, |mut row| {
                        row.col(|ui| {
                            ui.add_sized(
                                ui.available_size(),
                                egui::TextEdit::singleline(&mut item.name),
                            );
                        });

                        row.col(|ui| {
                            if ui.button("📋").clicked() {
                                ui.output_mut(|o| {
                                    o.commands
                                        .push(OutputCommand::CopyText(item.code.to_string()))
                                });
                            }

                            if ui.button("✖").clicked() {
                                remove_item = true;
                                remove_idx = idx;
                            }
                        });

                        row.col(|ui| {
                            ui.add_sized(
                                ui.available_size(),
                                egui::TextEdit::singleline(&mut item.code),
                            );
                        });
                    });
                }
            }
        });

    if remove_item {
        storage.workspace.runners.remove(remove_idx);
    }
}
