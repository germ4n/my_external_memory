use super::{persistence, Buffers, MemApp, Tab, TabEnum};

/// UI for one Idea
pub fn ideas_gui(ui: &mut egui::Ui, idea_id: usize, storage: &mut Buffers) {
    if idea_id >= storage.workspace.ideas.len() {
        return;
    }

    ui.horizontal(|ui| {
        ui.text_edit_singleline(&mut storage.workspace.ideas[idea_id].name);

        if ui.button("✖").clicked() {
            storage.idea_to_remove = Some(idea_id);
        }
    });

    egui::ScrollArea::vertical().show(ui, |ui| {
        ui.add(
            egui::TextEdit::multiline(&mut storage.workspace.ideas[idea_id].content)
                .font(egui::TextStyle::Monospace)
                .desired_rows(20)
                .lock_focus(true)
                .desired_width(f32::INFINITY),
        );
    });
}

/// UI for List of Ideas
pub fn ideas_list_gui(ui: &mut egui::Ui, storage: &mut MemApp) {
    ui.menu_button("🖹 Ideas", |ui| {
        ui.set_min_width(150.0);
        // New idea
        if ui.button("➕ New").clicked() {
            // Handle the "Open" button click
            storage.buffers.workspace.ideas.push(persistence::Idea {
                name: "new idea".to_owned(),
                content: "".to_owned(),
            });
            storage.dock_state.push_to_focused_leaf(Tab {
                tab_type: TabEnum::Ideas,
                idea_idx: storage.buffers.workspace.ideas.len() - 1,
                force_close: false,
            });
            ui.close_menu();
        }
        // List of ideas sorted
        let mut ideas_list: Vec<(&String, usize)> = vec![];
        for (id, idea) in storage.buffers.workspace.ideas.iter().enumerate() {
            ideas_list.push((&idea.name, id));
        }
        ideas_list.sort_by_key(|(name, _id)| *name);
        for (_name, id) in ideas_list {
            if ui
                .button(build_idea_name_decorated(&storage.buffers, id))
                .clicked()
            {
                storage.dock_state.push_to_focused_leaf(Tab {
                    tab_type: TabEnum::Ideas,
                    idea_idx: id,
                    force_close: false,
                });
                ui.close_menu();
            }
        }
    });
}

// Idea name with icon and modified mark
pub fn build_idea_name_decorated(buffers: &Buffers, idea_idx: usize) -> String {
    if idea_idx < buffers.workspace.ideas.len() {
        let modified = if idea_idx >= buffers.workspace_stored.ideas.len()
            || buffers.workspace.ideas[idea_idx] != buffers.workspace_stored.ideas[idea_idx]
        {
            "•"
        } else {
            ""
        };
        format!(
            "{} {} {}",
            "🖹", buffers.workspace.ideas[idea_idx].name, modified
        )
    } else {
        "<removed>".into()
    }
}
