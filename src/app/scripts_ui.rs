use super::{persistence, Buffers, Output};
use crate::app::execution;
use egui::OutputCommand;

/// UI for Scripts
pub fn scripts_gui(ui: &mut egui::Ui, storage: &mut Buffers) {
    ui.horizontal(|ui| {
        if ui.button("➕").clicked() {
            storage.workspace.scripts.push(persistence::Script {
                name: "".to_owned(),
                code: "".to_owned(),
                runner: "".to_owned(),
            });
        }
        ui.separator();
        ui.label("Filter: ");
        ui.text_edit_singleline(&mut storage.filter);
    });

    ui.separator();

    scripts_table_gui(ui, storage);
}

fn scripts_table_gui(ui: &mut egui::Ui, storage: &mut Buffers) {
    use egui_extras::{Column, TableBuilder};

    let mut remove_item = false;
    let mut remove_idx = 0;

    let table = TableBuilder::new(ui)
        .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
        .column(Column::auto())
        .column(Column::auto())
        .column(Column::auto())
        .column(Column::remainder())
        .min_scrolled_height(0.0);

    table
        .header(20.0, |mut header| {
            header.col(|ui| {
                ui.strong("Name");
            });
            header.col(|ui| {
                ui.strong("Actions");
            });
            header.col(|ui| {
                ui.strong("Runner");
            });
            header.col(|ui| {
                ui.strong("Code");
            });
        })
        .body(|mut body| {
            for (idx, item) in storage.workspace.scripts.iter_mut().enumerate() {
                if item.name.contains(&storage.filter) {
                    body.row(18.0, |mut row| {
                        row.col(|ui| {
                            ui.add(egui::TextEdit::singleline(&mut item.name));
                        });

                        row.col(|ui| {
                            if ui.button("▶").clicked() {
                                let mut output = "".to_owned();
                                if !item.runner.is_empty() {
                                    let f = storage
                                        .workspace
                                        .runners
                                        .iter()
                                        .find(|x| x.name == item.runner);
                                    match f {
                                        Some(runner) => {
                                            let full = format!("{} {}", &runner.code, &item.code);
                                            output = execution::run_script(&full);
                                        }
                                        None => println!("runner do not exist!"),
                                    }
                                } else {
                                    output = execution::run_script(&item.code);
                                }
                                storage.output.content = output;
                                storage.output.modified = true;
                            }

                            if ui.button("📋").clicked() {
                                ui.output_mut(|o| {
                                    o.commands
                                        .push(OutputCommand::CopyText(item.code.to_string()))
                                });
                            }

                            if ui.button("✖").clicked() {
                                remove_item = true;
                                remove_idx = idx;
                            }
                        });

                        row.col(|ui| {
                            egui::ComboBox::from_id_salt(idx)
                                .selected_text(item.runner.to_owned())
                                .show_ui(ui, |ui| {
                                    ui.selectable_value(
                                        &mut item.runner,
                                        "".to_owned(),
                                        "none".to_owned(),
                                    );
                                    for runner in &storage.workspace.runners {
                                        ui.selectable_value(
                                            &mut item.runner,
                                            runner.name.to_owned(),
                                            runner.name.to_owned(),
                                        );
                                    }
                                });
                        });

                        row.col(|ui| {
                            ui.add_sized(
                                ui.available_size(),
                                egui::TextEdit::singleline(&mut item.code),
                            );
                        });
                    });
                }
            }
        });

    if remove_item {
        storage.workspace.scripts.remove(remove_idx);
    }
}

/// UI for Scripts Output
pub fn script_output_gui(ui: &mut egui::Ui, output: &mut Output) {
    ui.add(
        egui::TextEdit::multiline(&mut output.content.as_str())
            .desired_width(f32::INFINITY)
            .desired_rows(10)
            .font(egui::TextStyle::Monospace)
            .lock_focus(true),
    );
    if output.modified {
        ui.scroll_to_cursor(Some(egui::Align::BOTTOM));
        output.modified = false;
    }
}
