use std::{
    os::windows::process::CommandExt,
    process::{Command, Stdio},
};

/// Execute an script code and return the result
pub fn run_script(script: &str) -> String {
    let tokens: Vec<&str> = script.split_whitespace().collect();
    if tokens.is_empty() {
        return "Nothing tu run".to_owned();
    }

    const CREATE_NO_WINDOW: u32 = 0x08000000;
    let mut command = Command::new(tokens[0]);
    command.args(&tokens[1..]);
    command.creation_flags(CREATE_NO_WINDOW);
    command.stdout(Stdio::piped());

    match command.output() {
        Ok(output) => (String::from_utf8_lossy(&output.stderr)
            + String::from_utf8_lossy(&output.stdout))
        .to_string(),
        Err(err) => format!("Failed to execute: {}", err.to_string()),
    }
}
