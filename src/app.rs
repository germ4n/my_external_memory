use egui_dock::{DockArea, DockState, Style};
use std::path::Path;

mod execution;
mod ideas_ui;
mod persistence;
mod runners_ui;
mod scripts_ui;

#[derive(serde::Deserialize, serde::Serialize, PartialEq)]
pub enum TabEnum {
    File,
    Ideas,
    Scripts,
    Output,
    Runners,
}

impl Tab {
    fn as_str(&self) -> &'static str {
        match self.tab_type {
            TabEnum::File => "💾 File",
            TabEnum::Ideas => "🖹 Ideas",
            TabEnum::Scripts => "🌟 Scripts",
            TabEnum::Output => "📃 Output",
            TabEnum::Runners => "🌠 Runners",
        }
    }
}

#[derive(serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Tab {
    tab_type: TabEnum,
    idea_idx: usize,
    force_close: bool,
}

pub struct Output {
    content: String,
    modified: bool,
}

#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct Buffers {
    /// Workspace data file name
    file_name: String,

    #[serde(skip)]
    workspace: persistence::Workspace,
    #[serde(skip)]
    workspace_stored: persistence::Workspace,

    #[serde(skip)]
    filter: String,
    #[serde(skip)]
    output: Output,

    #[serde(skip)]
    modified: bool,

    #[serde(skip)]
    idea_to_remove: Option<usize>,
}

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct MemApp {
    dock_state: DockState<Tab>,

    buffers: Buffers,
}

impl egui_dock::TabViewer for Buffers {
    type Tab = Tab;

    fn id(&mut self, tab: &mut Self::Tab) -> egui::Id {
        egui::Id::new(format!("{}-{}", tab.as_str(), tab.idea_idx))
    }

    fn title(&mut self, tab: &mut Self::Tab) -> egui::WidgetText {
        if tab.tab_type == TabEnum::Ideas {
            ideas_ui::build_idea_name_decorated(self, tab.idea_idx).into()
        } else {
            tab.as_str().into()
        }
    }

    fn ui(&mut self, ui: &mut egui::Ui, tab: &mut Self::Tab) {
        match tab.tab_type {
            TabEnum::File => file_gui(ui, self),
            TabEnum::Ideas => ideas_ui::ideas_gui(ui, tab.idea_idx, self),
            TabEnum::Scripts => scripts_ui::scripts_gui(ui, self),
            TabEnum::Output => scripts_ui::script_output_gui(ui, &mut self.output),
            TabEnum::Runners => runners_ui::runners_gui(ui, self),
        }
    }

    fn allowed_in_windows(&self, _tab: &mut Self::Tab) -> bool {
        false
    }

    fn force_close(&mut self, tab: &mut Self::Tab) -> bool {
        tab.force_close
    }
}

impl Default for Buffers {
    fn default() -> Self {
        let workspace = persistence::Workspace {
            runners: vec![],
            scripts: vec![],
            ideas: vec![],
        };
        Self {
            file_name: "Workspace.json".to_owned(),
            workspace: workspace.clone(),
            workspace_stored: workspace,
            filter: "".to_owned(),
            output: Output {
                content: "".to_owned(),
                modified: false,
            },
            modified: false,
            idea_to_remove: None,
        }
    }
}

impl Default for MemApp {
    fn default() -> Self {
        let tree = DockState::new(vec![Tab {
            tab_type: TabEnum::File,
            idea_idx: 0,
            force_close: false,
        }]);

        // Tree can be modified before constructing the dock with tree.main_surface_mut().split_xxx

        Self {
            dock_state: tree,
            buffers: Buffers::default(),
        }
    }
}

impl MemApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            let mut cfg: MemApp = eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();

            // Load last used workspace
            match persistence::load_json(&cfg.buffers.file_name) {
                Ok(data) => {
                    cfg.buffers.workspace_stored = data.clone();
                    cfg.buffers.workspace = data;
                }
                Err(err) => eprintln!("Error loading JSON: {}", err),
            }

            return cfg;
        }

        Default::default()
    }
}

fn menu_gui(ctx: &egui::Context, storage: &mut MemApp) {
    egui::TopBottomPanel::top("menu_top_panel").show(ctx, |ui| {
        // Ctrl+S shortcut
        if ui.input_mut(|i| i.consume_key(egui::Modifiers::COMMAND, egui::Key::S)) {
            save_workspace(&mut storage.buffers);
        }

        egui::menu::bar(ui, |ui| {
            // Workspace name
            let path = Path::new(&storage.buffers.file_name);
            let name = match path.with_extension("").to_str() {
                None => "new".to_string(),
                Some(str) => str.to_string(),
            };
            let modified = if storage.buffers.modified
                || storage.buffers.workspace != storage.buffers.workspace_stored
            {
                "•"
            } else {
                ""
            };
            let workspace_name = format!("{} {} {}", "💾", name, modified);
            if ui.selectable_label(false, workspace_name).clicked() {
                storage.dock_state.push_to_focused_leaf(Tab {
                    tab_type: TabEnum::File,
                    idea_idx: 0,
                    force_close: false,
                });
            }

            ui.separator();

            ideas_ui::ideas_list_gui(ui, storage);

            ui.separator();

            if ui.selectable_label(false, "🌟 Scripts").clicked() {
                storage.dock_state.push_to_focused_leaf(Tab {
                    tab_type: TabEnum::Scripts,
                    idea_idx: 0,
                    force_close: false,
                });
            }
            if ui.selectable_label(false, "📃 Output").clicked() {
                storage.dock_state.push_to_focused_leaf(Tab {
                    tab_type: TabEnum::Output,
                    idea_idx: 0,
                    force_close: false,
                });
            }
            if ui.selectable_label(false, "🌠 Runners").clicked() {
                storage.dock_state.push_to_focused_leaf(Tab {
                    tab_type: TabEnum::Runners,
                    idea_idx: 0,
                    force_close: false,
                });
            }

            ui.with_layout(egui::Layout::bottom_up(egui::Align::RIGHT), |ui| {
                egui::warn_if_debug_build(ui);
            });
        });
    });
}

fn close_removed_idea(storage: &mut MemApp) {
    match storage.buffers.idea_to_remove {
        Some(idea_to_remove) => {
            // Remove data
            storage.buffers.workspace.ideas.remove(idea_to_remove);
            if storage.buffers.workspace_stored.ideas.len() > idea_to_remove {
                storage
                    .buffers
                    .workspace_stored
                    .ideas
                    .remove(idea_to_remove);
                storage.buffers.modified = true;
            }

            // Update tabs
            for tab in storage.dock_state.iter_all_tabs_mut() {
                if tab.1.tab_type == TabEnum::Ideas {
                    if tab.1.idea_idx == idea_to_remove {
                        tab.1.force_close = true;
                    } else if tab.1.idea_idx > idea_to_remove {
                        tab.1.idea_idx -= 1;
                    }
                }
            }

            storage.buffers.idea_to_remove = None;
        }
        None => {}
    }
}

impl eframe::App for MemApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        close_removed_idea(self);
        menu_gui(ctx, self);
        DockArea::new(&mut self.dock_state)
            .show_close_buttons(true)
            .style(Style::from_egui(ctx.style().as_ref()))
            .show(ctx, &mut self.buffers);
    }
}

fn save_workspace(storage: &mut Buffers) {
    let _ = persistence::save_json(&storage.file_name, &storage.workspace);
    storage.workspace_stored = storage.workspace.clone();
}

fn file_gui(ui: &mut egui::Ui, storage: &mut Buffers) {
    ui.heading("Workspace File");

    ui.horizontal(|ui| {
        ui.label("File: ");
        ui.text_edit_singleline(&mut storage.file_name);
    });

    ui.horizontal(|ui| {
        if ui.button("🗁 Load").clicked() {
            match persistence::load_json(&storage.file_name) {
                Ok(data) => {
                    storage.workspace_stored = data.clone();
                    storage.workspace = data;
                    storage.modified = false;
                }
                Err(err) => eprintln!("Error loading JSON: {}", err),
            }
        }
        if ui.button("💾 Save").clicked() {
            save_workspace(storage);
        }
    });
}
